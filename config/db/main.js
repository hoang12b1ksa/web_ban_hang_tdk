const mongoose = require('mongoose');
async function connect() {
  try {
    await mongoose.connect('mongodb://localhost:27017/web_ban_hang_tdk', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    //console.log("connect Successfully")
  } catch (error) {
    console.log('connect failture');
  }
}
module.exports = { connect };
