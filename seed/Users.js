const mongoose = require('mongoose');
const Users = require('../models/Users');
// Tự connect tới database
mongoose.connect('mongodb://localhost:27017/web_ban_hang_tdk', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
// Xóa toàn bộ dữ liệu
Users.deleteMany()
        .then(data=>{
          console.log("Users seed cleared");
        })
        .catch(err=>{
          console.log(err);
        })
//Tạo dữ liệu mới


Users.create({
    name: "Trịnh Đăng Khoa",
    address: "Hà Nội",
    account: "khoatd@gmail.com",
    password: "123456789",
    phone: "0912345678",
    token: null
},{
    name: "Trịnh Đăng Khoa 1",
    address: "Hà Nội",
    account: "khoatd1@gmail.com",
    password: "123456789",
    phone: "0912345678",
    token: null
},{
    name: "Trịnh Đăng Khoa 2",
    address: "Hà Nội",
    account: "khoatd2@gmail.com",
    password: "123456789",
    phone: "0912345678",
    token: null
},{
    name: "Trịnh Đăng Khoa 3",
    address: "Hà Nội",
    account: "khoatd3@gmail.com",
    password: "123456789",
    phone: "0912345678",
    token: null
})
.then(data=>console.log(data))
.catch(err=> console.log(err))